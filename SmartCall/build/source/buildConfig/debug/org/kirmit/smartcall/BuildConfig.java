/**
 * Automatically generated file. DO NOT MODIFY
 */
package org.kirmit.smartcall;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String PACKAGE_NAME = "org.kirmit.smartcall";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
}
