package org.kirmit.smartcall.listener;

import android.content.Context;
import android.media.AudioManager;
import android.widget.SeekBar;

import org.kirmit.smartcall.widget.VerticalSeekBar;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kirmit on 08.12.13.
 */
public class OnVolumeLevelChangeListener implements
        VerticalSeekBar.OnSeekBarProgressChangeListener {

    private final List<Integer> mStreamTypes;
    private final AudioManager mManager;

    public OnVolumeLevelChangeListener(Context context, List<Integer> streamTypes) {
        mStreamTypes = new ArrayList<Integer>(streamTypes);
        mManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress) {
        for (Integer streamType: mStreamTypes) {
            int originValue = mManager.getStreamVolume(streamType);
            mManager.setStreamVolume(streamType, progress, AudioManager.FLAG_PLAY_SOUND);
            mManager.setStreamVolume(streamType, originValue, 0);
        }
    }

    @Override
    public void onStartProgress(SeekBar seekBar) {}

    @Override
    public void onStopProgress(SeekBar seekBar) {}
}
