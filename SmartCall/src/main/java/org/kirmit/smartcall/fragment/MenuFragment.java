package org.kirmit.smartcall.fragment;


import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import org.kirmit.smartcall.R;
import org.kirmit.smartcall.service.CallChangeVolumeStrategy;
import org.kirmit.smartcall.service.ChangeVolumeService;

/**
 * Created by kirmit on 08.12.13.
 */
public class MenuFragment extends ListFragment {

    private static final String STOP_SERVICE_TAG = "Stop Service";
    private static final String START_SERVICE_TAG = "Start Service";
    private static final String ABOUT_TAG = "About";
    private static final String HELP_TAG = "Help";

    private static interface OnMenuClickListener {
        public void onMenuItemClick();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        SampleAdapter adapter = new SampleAdapter(getActivity());
        if (isCallServiceRunning()) {
            adapter.add(new SampleItem(STOP_SERVICE_TAG, android.R.drawable.ic_menu_close_clear_cancel,
                    new OnMenuClickListener() {
                        @Override
                        public void onMenuItemClick() {

                        }
                    }));
        } else {
            adapter.add(new SampleItem(START_SERVICE_TAG, android.R.drawable.ic_menu_call,
                    new OnMenuClickListener() {
                        @Override
                        public void onMenuItemClick() {

                        }
                    }));
        }
        adapter.add(new SampleItem(ABOUT_TAG, android.R.drawable.ic_menu_preferences,
                new OnMenuClickListener() {
                    @Override
                    public void onMenuItemClick() {

                    }
                }));
        adapter.add(new SampleItem(HELP_TAG, android.R.drawable.ic_menu_help,
                new OnMenuClickListener() {
                    @Override
                    public void onMenuItemClick() {

                    }
                }));
        setListAdapter(adapter);
    }

    private class SampleItem {
        public String tag;
        public int iconRes;
        public final OnMenuClickListener onMenuClickListener;
        public SampleItem(String tag, int iconRes, OnMenuClickListener onMenuClickListener) {
            this.tag = tag;
            this.iconRes = iconRes;
            this.onMenuClickListener = onMenuClickListener;
        }
    }

    public class SampleAdapter extends ArrayAdapter<SampleItem> {

        public SampleAdapter(Context context) {
            super(context, 0);
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
            }
            ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
            icon.setImageResource(getItem(position).iconRes);
            TextView title = (TextView) convertView.findViewById(R.id.row_title);
            title.setText(getItem(position).tag);

            return convertView;
        }

    }

    private boolean isCallServiceRunning() {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (CallChangeVolumeStrategy.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        SampleItem  item = (SampleItem) getListAdapter().getItem(position);
        item.onMenuClickListener.onMenuItemClick();
        Log.d("[Menu Fragment]", "Menu Click " + item.tag);
        super.onListItemClick(l, v, position, id);
    }
}
