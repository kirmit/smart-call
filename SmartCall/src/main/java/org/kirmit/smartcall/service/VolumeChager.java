package org.kirmit.smartcall.service;

/**
 * Created by kirmit on 08.12.13.
 */
public class VolumeChager {

    private IChangeVolumeStrategy mStrategy;

    public void setStrategy(IChangeVolumeStrategy strategy) {
        mStrategy = strategy;
    }

    public void change(Double relate, int defaultVal) {
        mStrategy.change(relate, defaultVal);
    }
}
