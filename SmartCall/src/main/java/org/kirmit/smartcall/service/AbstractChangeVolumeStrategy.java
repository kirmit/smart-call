package org.kirmit.smartcall.service;

import android.media.AudioManager;

/**
 * Created by kirmit on 08.12.13.
 */
public abstract class AbstractChangeVolumeStrategy implements IChangeVolumeStrategy {

    protected AudioManager mAm;

    public AbstractChangeVolumeStrategy(AudioManager am) {
        mAm = am;
    }
}
