package org.kirmit.smartcall.service;

import android.media.AudioManager;

/**
 * Created by kirmit on 08.12.13.
 */
public class CallChangeVolumeStrategy extends AbstractChangeVolumeStrategy {

    public CallChangeVolumeStrategy(AudioManager am) {
        super(am);
    }

    @Override
    public void change(Double relate, int defaultVal) {
        int newValue = (int) (defaultVal*relate);
        mAm.setStreamVolume(AudioManager.STREAM_RING, newValue, 0);
        mAm.setStreamVolume(AudioManager.STREAM_MUSIC, newValue, 0);
    }
}
