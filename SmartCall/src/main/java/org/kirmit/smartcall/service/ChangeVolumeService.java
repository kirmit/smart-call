package org.kirmit.smartcall.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import org.kirmit.smartcall.R;
import org.kirmit.smartcall.TuneActivity;

import java.math.BigDecimal;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class ChangeVolumeService extends Service {

    public static final String SMS_RECEIVE_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private static final int FREQUENCY = 44100;
    private static final int EMUL_FREQUENCY = 8000;
    private static final int CHANNEL = AudioFormat.CHANNEL_IN_DEFAULT;
    private static final int ENCODING = AudioFormat.ENCODING_PCM_16BIT;
    private static final double CALIBRATION_VOLUME_VALUE = 0.000002;
    private static final int ONGOING_NOTIFICATION_ID = 1;
    //private Handler mWorkerHandler;
    //private HandlerThread mHandlerWorkerThread;
    private AtomicBoolean isAlreadyScheduled = new AtomicBoolean(false);

    private AtomicInteger mCallLevel = new AtomicInteger(0);
    private AtomicInteger mSmsLevel = new AtomicInteger(0);
    private AtomicInteger mAlarmLevel = new AtomicInteger(0);
    private AtomicReference<Double> mDefaultSpl = new AtomicReference<Double>(Double.valueOf(0));

    private AudioRecord mRecordInstance;
    private ScheduledExecutorService mExecutor;
    private VolumeChager volumeChager = new VolumeChager();
    private PhoneStateListener mPhoneStateListener = new PhoneStateListener() {
        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            if (state == TelephonyManager.CALL_STATE_RINGING) {
                mExecutor.execute(mVolumeUpdater);
            }
        }
    };
    private BroadcastReceiver mCallAndSmsReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            mExecutor.execute(mVolumeUpdater);
        }
    };
    private Runnable mVolumeUpdater = new Runnable() {
        @Override
        public void run() {
            Double currentSpl = measureRawSPLFromMicro(calculateBufferSize());
            BigDecimal relateSpl = new BigDecimal(currentSpl/mDefaultSpl.get());

            AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            volumeChager.setStrategy(new AlarmChangeVolumeStrategy(am));
            volumeChager.change(relateSpl.doubleValue(), mAlarmLevel.get());

            volumeChager.setStrategy(new CallChangeVolumeStrategy(am));
            volumeChager.change(relateSpl.doubleValue(), mAlarmLevel.get());

            volumeChager.setStrategy(new NotificationChangeVolumeStrategy(am));
            volumeChager.change(relateSpl.doubleValue(), mAlarmLevel.get());
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        /*mHandlerWorkerThread = new HandlerThread("ChangeVolumeExecutor");
        mHandlerWorkerThread.start();
        mWorkerHandler = new Handler(mHandlerWorkerThread.getLooper());*/

        TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        int events = PhoneStateListener.LISTEN_CALL_STATE;
        tm.listen(mPhoneStateListener, events);

        mRecordInstance = new AudioRecord(MediaRecorder.AudioSource.MIC,
                FREQUENCY, CHANNEL, ENCODING, calculateBufferSize());

        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        intentFilter.addAction(SMS_RECEIVE_ACTION);
        intentFilter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);
        registerReceiver(mCallAndSmsReceiver, intentFilter);

        mExecutor = Executors.newScheduledThreadPool(5);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        setDefaultLevels(intent);
        setDefaultSpl();
        if (isAlreadyScheduled.get()) {
            mExecutor.scheduleAtFixedRate(mVolumeUpdater, 0, 10, TimeUnit.SECONDS);
            startForegroundCompat();
        } else {
            mExecutor.execute(mVolumeUpdater);
        }
        return START_STICKY;
    }

    private void startForegroundCompat() {
        Intent notificationIntent = new Intent(this, TuneActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notification = createNotification(pendingIntent);
        } else {
            notification = new Notification(R.drawable.ic_launcher,
                    getText(R.string.notification_title), System.currentTimeMillis());
            notification.setLatestEventInfo(this, getText(R.string.notification_title),
                    getText(R.string.notification_text), pendingIntent);
        }
        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private Notification createNotification(PendingIntent pendingIntent) {
        Notification notification = new Notification.Builder(this)
                .setContentTitle(getText(R.string.notification_title))
                .setContentText(getText(R.string.notification_text))
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.notification_title))
                .build();
        return notification;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        mExecutor.shutdown();
        mRecordInstance.release();
        stopForeground(true);
        super.onDestroy();
    }

    private Double measureRawSPLFromMicro(int bufferSize) {
        Double rmsValue = 1.0;
        /*mRecordInstance.startRecording();

        short[] tempBuffer = new short[bufferSize];

        mRecordInstance.read(tempBuffer, 0, bufferSize);
        for (int i = 0; i < bufferSize - 1; i++) {
            rmsValue += tempBuffer[i] * tempBuffer[i];
        }
        rmsValue = rmsValue / bufferSize;
        rmsValue = Math.sqrt(rmsValue);*/

        Double splValue = 20 * Math.log10(rmsValue / CALIBRATION_VOLUME_VALUE);
        Log.d("Volume Service", "Raw spl value:" + splValue);

        return splValue;
    }

    private int calculateBufferSize() {
        int bufferSize = AudioRecord.getMinBufferSize(EMUL_FREQUENCY, CHANNEL, ENCODING) * 20;
        return bufferSize;
    }

    private void setDefaultLevels(Intent intent) {
        int callLevel = intent.getIntExtra(TuneActivity.CALL_VOLUME_LEVEL, 0);
        while (mCallLevel.compareAndSet(mCallLevel.get(), callLevel)) {
        }
        int smsLevel = intent.getIntExtra(TuneActivity.SMS_VOLUME_LEVEL, 0);
        while (mSmsLevel.compareAndSet(mSmsLevel.get(), smsLevel)){
        }
        int alarmLevel = intent.getIntExtra(TuneActivity.ALARM_VOLUME_LEVEL, 0);
        while (mAlarmLevel.compareAndSet(mAlarmLevel.get(), alarmLevel)){
        }
    }

    private void setDefaultSpl() {
        Double currentSpl = measureRawSPLFromMicro(calculateBufferSize());
        while (!mDefaultSpl.compareAndSet(mDefaultSpl.get(), currentSpl)) {
            currentSpl = measureRawSPLFromMicro(calculateBufferSize());
            Log.d("ChangerVolume", "Error in set value");
        }
    }
}

