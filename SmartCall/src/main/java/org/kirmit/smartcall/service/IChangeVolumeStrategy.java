package org.kirmit.smartcall.service;

/**
 * Created by kirmit on 08.12.13.
 */
public interface IChangeVolumeStrategy {
    public void change(Double relate, int defaultVal);
}
