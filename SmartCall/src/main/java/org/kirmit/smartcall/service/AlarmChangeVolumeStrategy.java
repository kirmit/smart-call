package org.kirmit.smartcall.service;

import android.media.AudioManager;

/**
 * Created by kirmit on 08.12.13.
 */
public class AlarmChangeVolumeStrategy extends AbstractChangeVolumeStrategy {

    public AlarmChangeVolumeStrategy(AudioManager am) {
        super(am);
    }

    @Override
    public void change(Double relate, int defaultVal) {
        int newValue = (int) (defaultVal*relate);
        mAm.setStreamVolume(AudioManager.STREAM_ALARM, newValue, 0);
        mAm.setStreamVolume(AudioManager.STREAM_SYSTEM, newValue, 0);
    }
}
