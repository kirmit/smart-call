package org.kirmit.smartcall.service;

import android.media.AudioManager;

/**
 * Created by kirmit on 08.12.13.
 */
public class NotificationChangeVolumeStrategy extends AbstractChangeVolumeStrategy {

    public NotificationChangeVolumeStrategy(AudioManager am) {
        super(am);
    }

    @Override
    public void change(Double relate, int defaultVal) {
        int newValue = (int) (defaultVal*relate);
        mAm.setStreamVolume(AudioManager.STREAM_NOTIFICATION, newValue, 0);
    }
}
