package org.kirmit.smartcall;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import com.actionbarsherlock.view.MenuItem;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;

import org.kirmit.smartcall.fragment.MenuFragment;
import org.kirmit.smartcall.listener.OnVolumeLevelChangeListener;
import org.kirmit.smartcall.service.ChangeVolumeService;
import org.kirmit.smartcall.widget.VerticalSeekBar;

import java.util.Arrays;

public class TuneActivity extends SlidingFragmentActivity {

    public static final String ALARM_VOLUME_LEVEL = "alarm_level";
    public static final String SMS_VOLUME_LEVEL = "sms_level";
    public static final String CALL_VOLUME_LEVEL = "call_level";

    private Button mCalibrateButton;
    private VerticalSeekBar mAlarmSeekBar;
    private VerticalSeekBar mSmsSeekBar;
    private VerticalSeekBar mCallSeekBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSlidingActionBarEnabled(false);
        setContentView(R.layout.activity_tune);

        initSlidingMenu(savedInstanceState);
        initUI();
        setVolumeLevel();
        initListeners();
        
    }

    private void initSlidingMenu(Bundle savedInstanceState) {
        setBehindContentView(R.layout.menu_frame);

        SlidingMenu slidingMenu = getSlidingMenu();

        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.setShadowWidthRes(R.dimen.shadow_width);
        slidingMenu.setShadowDrawable(R.drawable.shadow);
        slidingMenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidingMenu.setFadeDegree(0.5f);
        slidingMenu.setBehindScrollScale(0.5f);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.menu_frame, new MenuFragment())
                .commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home :
                toggle();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initUI() {
        mAlarmSeekBar = (VerticalSeekBar) findViewById(R.id.verical_seekbar_alarm);
        mSmsSeekBar = (VerticalSeekBar) findViewById(R.id.verical_seekbar_sms);
        mCallSeekBar = (VerticalSeekBar) findViewById(R.id.verical_seekbar_call);

        mCalibrateButton = (Button) findViewById(R.id.button_calibrate);
        mCalibrateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calibrate();
            }
        });

    }

    private void setVolumeLevel() {
        AudioManager audio = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mAlarmSeekBar.setMax(audio.getStreamMaxVolume(AudioManager.STREAM_ALARM));
        mAlarmSeekBar.setProgress(audio.getStreamVolume(AudioManager.STREAM_ALARM));
        mCallSeekBar.setMax(audio.getStreamMaxVolume(AudioManager.STREAM_RING));
        mCallSeekBar.setProgress(audio.getStreamVolume(AudioManager.STREAM_RING));
        mSmsSeekBar.setMax(audio.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION));
        mSmsSeekBar.setProgress(audio.getStreamVolume(AudioManager.STREAM_NOTIFICATION));
    }

    private void initListeners() {
        mCallSeekBar.setOnSeekBarChangeListener(
                new OnVolumeLevelChangeListener(this,
                        Arrays.asList(AudioManager.STREAM_RING, AudioManager.STREAM_MUSIC))
        );
        mAlarmSeekBar.setOnSeekBarChangeListener(
                new OnVolumeLevelChangeListener(this,
                        Arrays.asList(AudioManager.STREAM_SYSTEM, AudioManager.STREAM_ALARM))
        );
        mSmsSeekBar.setOnSeekBarChangeListener(
                new OnVolumeLevelChangeListener(this,
                        Arrays.asList(AudioManager.STREAM_NOTIFICATION))
        );
    }

    private void calibrate() {
        Intent serviceIntent = new Intent(this, ChangeVolumeService.class)
                .putExtra(ALARM_VOLUME_LEVEL, mAlarmSeekBar.getProgress())
                .putExtra(SMS_VOLUME_LEVEL, mSmsSeekBar.getProgress())
                .putExtra(CALL_VOLUME_LEVEL, mCallSeekBar.getProgress());
        //startService(serviceIntent);
    }


}
