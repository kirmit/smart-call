package org.kirmit.smartcall.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.SeekBar;

public class VerticalSeekBar extends SeekBar {

    private Context mContext;

    private boolean mPositionCalibrated = false;

    private OnSeekBarProgressChangeListener mOnSeekBarChangeListener;

    public interface OnSeekBarProgressChangeListener {
        public void onProgressChanged(SeekBar seekBar, int progress);

        public void onStartProgress(SeekBar seekBar);

        public void onStopProgress(SeekBar seekBar);
    }

    private OnSeekBarChangeListener mOnSeekBarChangeListenerStd = new OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            if (mOnSeekBarChangeListener != null) {
                mOnSeekBarChangeListener.onProgressChanged(seekBar, progress);
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {}

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {}
    };

    public VerticalSeekBar(Context context) {
        super(context);
        mContext = context;
        setOnSeekBarChangeListener(mOnSeekBarChangeListenerStd);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        mContext = context;
        setOnSeekBarChangeListener(mOnSeekBarChangeListenerStd);
    }

    public VerticalSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        setOnSeekBarChangeListener(mOnSeekBarChangeListenerStd);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(h, w, oldh, oldw);
        if (!mPositionCalibrated) {
            View parentView = (View) getParent();
            int translateX = (parentView.getWidth() / 3 - getWidth()) / 2;

            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) getLayoutParams();
            params.setMargins(translateX, 0, translateX, 0);
            setLayoutParams(params);
            mPositionCalibrated = true;
        }
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec,
            int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {

        
        canvas.rotate(270);
        canvas.translate(-getHeight(), 0);
        super.onDraw(canvas);
    }

    public void setOnSeekBarChangeListener(
            OnSeekBarProgressChangeListener listener) {
        if (listener != null) {
            mOnSeekBarChangeListener = listener;
        }
    }

    private void onStartProgress() {
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onStartProgress(this);
        }
    }

    private void onStopProgress() {
        if (mOnSeekBarChangeListener != null) {
            mOnSeekBarChangeListener.onStopProgress(this);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!isEnabled()) {
            return false;
        }

        switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN:
            onStartProgress();
            handleMove(event);
            break;
        case MotionEvent.ACTION_UP:
            handleMove(event);
            onStopProgress();
            break;
        case MotionEvent.ACTION_MOVE:
            handleMove(event);
            break;

        case MotionEvent.ACTION_CANCEL:
            break;
        }
        return true;
    }

    private void handleMove(MotionEvent event) {
        setProgress(getMax() - (int) (getMax() * event.getY() / getHeight()));
        onSizeChanged(getWidth(), getHeight(), 0, 0);
    }
}
